FROM alpine:3.7

ENV TERM xterm

#Creación de la base de datos y restore:
COPY create_db.sql /tmp/create_db.sql
COPY bugtracker_restore.sql /tmp/bugtracker_restore.sql

#Agrega bash
RUN apk update && apk add bash

#Runit:
RUN apk update && apk add runit

#Java:
ENV JAVA_VERSION_MAJOR 8
ENV JAVA_VERSION_MINOR 144
ENV JAVA_VERSION_BUILD 14
ENV JAVA_PACKAGE jdk
ENV JAVA_HOME /opt/jdk
ENV PATH ${PATH}:${JAVA_HOME}/bin
RUN apk update && \
	apk --update add curl ca-certificates tar && \
	wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://raw.githubusercontent.com/sgerrand/alpine-pkg-glibc/master/sgerrand.rsa.pub && \
	cd /tmp && \
	wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.25-r0/glibc-2.25-r0.apk && \
	apk add glibc-2.25-r0.apk && \
	mkdir /opt && \
	cd /opt && \
	wget http://rpcloud.com.ar/jdk-8u144-linux-x64.tar.gz && \
	tar xzf jdk-8u144-linux-x64.tar.gz && \
	rm -f jdk-8u144-linux-x64.tar.gz && \
	ln -s /opt/jdk1.${JAVA_VERSION_MAJOR}.0_${JAVA_VERSION_MINOR} /opt/jdk && \
	rm -fr /opt/jdk/*src.zip \
		/opt/jdk/lib/missioncontrol \
		/opt/jdk/lib/visualvm \
		/opt/jdk/lib/*javafx* \
		/opt/jdk/jre/lib/plugin.jar \
		/opt/jdk/jre/lib/ext/jfxrt.jar \
		/opt/jdk/jre/bin/javaws \
		/opt/jdk/jre/lib/javaws.jar \
		/opt/jdk/jre/lib/desktop \
		/opt/jdk/jre/plugin \
		/opt/jdk/jre/lib/deploy* \
		/opt/jdk/jre/lib/*javafx* \
		/opt/jdk/jre/lib/*jfx* \
		/opt/jdk/jre/lib/amd64/libdecora_sse.so \
		/opt/jdk/jre/lib/amd64/libprism_*.so \
		/opt/jdk/jre/lib/amd64/libfxplugins.so \
		/opt/jdk/jre/lib/amd64/libglass.so \
		/opt/jdk/jre/lib/amd64/libgstreamer-lite.so \
		/opt/jdk/jre/lib/amd64/libjavafx*.so \
		/opt/jdk/jre/lib/amd64/libjfx*.so && \
	echo "JAVA_VERSION_MAJOR=8" >> /etc/profile.d/java.sh && \
	echo "JAVA_VERSION_MINOR=144" >> /etc/profile.d/java.sh && \
	echo "JAVA_VERSION_BUILD=14" >> /etc/profile.d/java.sh && \
	echo "JAVA_PACKAGE=jdk" >> /etc/profile.d/java.sh && \
	echo "JAVA_HOME=/opt/jdk" >> /etc/profile.d/java.sh && \
	echo "PATH=${PATH}:${JAVA_HOME}/bin" >> /etc/profile.d/java.sh && \
	apk add paxctl && \
	paxctl -c /opt/jdk/bin/java && \
	paxctl -m /opt/jdk/bin/java

#Liquibase:
RUN mkdir /root/liquibase
COPY liquibase-3.5.3-bin.zip /root/liquibase/liquibase-3.5.3-bin.zip
COPY mysql-connector-java-5.1.44.zip /root/liquibase/mysql-connector-java-5.1.44.zip
RUN cd /root/liquibase && \
	unzip /root/liquibase/liquibase-3.5.3-bin.zip && \
	rm -f /root/liquibase/liquibase-3.5.3-bin.zip && \
	unzip /root/liquibase/mysql-connector-java-5.1.44.zip && \
	rm -f /root/liquibase/mysql-connector-java-5.1.44.zip

RUN apk update && apk add mysql-client

RUN apk update && \
	apk add supervisor && \
	mkdir -p /var/log/supervisor

ADD ./init.sh /root/run.sh
RUN chmod +x /root/run.sh
ENTRYPOINT ["/root/run.sh"]
